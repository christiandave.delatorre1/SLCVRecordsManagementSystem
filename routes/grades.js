import express from "express";
import { gradesController, createStudentData } from "../controllers/grades.js";

const router = express.Router();

router.get('/', gradesController);
router.post('/', createStudentData)

export default router;