import express from 'express';
import {getMonitoringRequest, createMonitoringRequest,  updateMonitoringRequest, deleteMonitoringRequest } from '../controllers/monitoring.js';

const router = express.Router();

router.get('/', getMonitoringRequest);
router.post('/', createMonitoringRequest);
router.patch('/:id', updateMonitoringRequest);
router.delete('/:id', deleteMonitoringRequest);


export default router;