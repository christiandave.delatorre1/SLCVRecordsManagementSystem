import express from 'express';
import {getStudentDataBySearch, getStudentData, createStudentData, updateStudentData } from '../controllers/student.js'

const router = express.Router();

router.get('/', getStudentData);
router.get('/search', getStudentDataBySearch)
router.post('/', createStudentData);
router.patch('/:id', updateStudentData)

export default router;