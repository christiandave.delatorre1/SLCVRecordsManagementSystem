import express from "express";
import multer from "multer";
import {curriculumController, getStudentCurriculumBySearch, createStudentData} from '../controllers/curriculum.js';
// import { fileURLToPath } from 'url';
// import { dirname } from 'path';
// import bodyParser from "body-parser";
// import path from 'path';
// const app = express();

// // const app = express();
const router = express.Router();
// const __filename = fileURLToPath(import.meta.url);
// const __dirname = dirname(__filename);
// const test = path.resolve(__filename, 'public')
// console.log(__filename)

// app.use(express.static(path.resolve(__dirname, 'public')));


// const storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//         cb(null,'public/csv');
//     },
//     filename: (req, file, cb) => {
//         cb(null, file.originalname)
//     }
// })
// console.log(storage)

// const upload = multer({ storage: storage});

// router.post('/', upload.single('file'), curriculumController);
router.get('/', curriculumController)
router.get('/search', getStudentCurriculumBySearch)
router.post('/', createStudentData)


export default router;