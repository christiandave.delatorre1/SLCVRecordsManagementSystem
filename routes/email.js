import express from 'express';
import { emailController } from '../controllers/email.js';

const router = express.Router();

router.post('/', emailController);

export default router;