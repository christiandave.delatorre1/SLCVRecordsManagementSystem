import grades from "../models/grades.js";
export const gradesController = async (req, res) => {
  // console.log(req.file)
  try {
    const data = await grades.find();
    res.status(200).json(data);
  } catch (error) {
    res.send({ status: 400, success: false, msg: req.file });
  }
};

export const createStudentData = async (req, res) => {
  const students = req.body;
  const filteredArray = students.filter(obj => obj !== null);
  console.log(filteredArray, 'filtered req body')
  try {
    const result = await grades.insertMany(filteredArray);
    res.status(201).json(result);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};
