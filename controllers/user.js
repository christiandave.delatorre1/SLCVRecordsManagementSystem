import User from '../models/user.js';

export const signin = async(req, res) => {
    const {userName, password} = req.body;
    try {
        const existingUser = await User.findOne({ userName });

        if(!existingUser) return res.status(404).json({message: "User doesn't exist"});

        const isPasswordCorrect = password === existingUser.password;

        console.log(isPasswordCorrect);
        console.log(existingUser);

        if(!isPasswordCorrect) return res.status(400).json({message: "Invalid Credentials."});

        res.status(200).json({ message: "Successfully logged In" });

    } catch (error) {
        res.status(500).json({message: "Something went wrong", error});
        
    }

}
