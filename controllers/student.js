import studentData from '../models/student.js';
import mongoose from 'mongoose';

export const getStudentData = async (req, res) => {
    try {
        const studentInfo = await studentData.find();
        res.status(200).json(studentInfo);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const getStudentDataBySearch = async (req, res) => {
    const { searchQuery } = req.query;

    try {
        const title = new RegExp(searchQuery, 'i');
        const student = await studentData.find({ $or: [ {familyName: title}, {givenName: title}, {middleName: title}, {studentNumber: title} ]});
        res.json({ data: student})
    } catch (error) {
        res.status(404).json({ message: error.message})
    }
}

export const createStudentData = async (req, res) => {
    const student = req.body;
    const newStudent = new studentData(student);

    try {
        await newStudent.save();
        res.status(201).json(newStudent);
    } catch (error) {
        res.status(404).json({ message: error.message})
    }
}

export const updateStudentData = async (req, res) => {
    const { id } = req.params;
    const updates = req.body;
    try {
        if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send("No Item found");
        const updatedItem = await studentData.findByIdAndUpdate(id, {...updates, id}, {new: true});
        res.json(updatedItem);
    } catch (error) {
        res.json({message: error.message});
    }
}

