import curriculum from "../models/curriculum.js";
// import csv from "csvtojson";

export const curriculumController = async (req, res) => {
  // console.log(req.file)
  try {
    const data = await curriculum.find();
    res.status(200).json(data);
  } catch (error) {
    res.send({ status: 400, success: false, msg: req.file });
  }
};

export const getStudentCurriculumBySearch = async (req, res) => {
  const { searchQuery } = req.query;

  try {
      const title = new RegExp(searchQuery, 'i');
      const student = await curriculum.find({ $or: [ {Course: title}, {SchoolYear: title}, {Semester: title} ]});
      res.json({ data: student})
  } catch (error) {
      res.status(404).json({ message: error.message})
  }
}

export const createStudentData = async (req, res) => {
  const students = req.body;
  const filteredArray = students.filter(obj => obj !== null);
  try {
    const result = await curriculum.insertMany(filteredArray);
    res.status(201).json(result);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};


