import nodemailer from 'nodemailer';
import * as dotenv from 'dotenv';
dotenv.config();

export const emailController = async (req, res) => {
  const { to, subject, html } = req.body;
  console.log(req.body)
  console.log(to)

  let transporter = nodemailer.createTransport({
    service: "gmail",
    port: 465,
    secure: true,
    logger: true,
    debug: true,
    secureConnection: false,
    auth: {
      user: 'slcv2024@gmail.com',
      pass: process.env.PASSWORD,
    },
    
  });

  const emailOptions = {
    from: 'slcv2024@gmail.com',
    to,
    subject,
    html,
  };
  
  const sendEmail = async (emailOptions) => {
    try {
      await transporter.sendMail(emailOptions);
      console.log("Email sent successfully!");
    } catch (error) {
      console.log("Error sending email:", error);
    }
  };

  

  try {
    await sendEmail(emailOptions);
    res.status(200).json({ message: "Email sent successfully!" });
  } catch (error) {
    res.status(500).json({ message: "Error sending email" });
  }
};
