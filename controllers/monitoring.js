import monitoring from "../models/monitoring.js";
import mongoose from "mongoose";

export const getMonitoringRequest = async(req, res) => {
    try {
        const monitoringRequest = await monitoring.find();
        res.status(200).json(monitoringRequest);
    } catch (error) {
        res.status(404).json({message: error.message});
    }
}

export const createMonitoringRequest = async(req, res) => {
    const data = req.body;
    const newMonitoringRequest = new monitoring(data);

    try {
        await newMonitoringRequest.save();
    } catch (error) {
        res.status(404).json({message: error.message});
    }
}

export const updateMonitoringRequest = async(req, res) => {
    const { id } = req.params;
    const updates = req.body;
    try {
        if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send("No Item found");
        const updatedItem = await monitoring.findByIdAndUpdate(id, {...updates, id}, { new: true } );
        res.json(updatedItem);
    } catch (error) {
        res.json({message: error.message});
    }
}

export const deleteMonitoringRequest = async(req, res) => {
    const { id } = req.params;
    try {
        if(!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send("No id found to be deleted");
        await monitoring.findByIdAndDelete(id);
        res.json({message: "Request deleted"});

    } catch (error) {
        res.json({message: error.message})
    }
}