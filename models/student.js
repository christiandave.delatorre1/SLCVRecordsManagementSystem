import mongoose from "mongoose";

const studentSchema = mongoose.Schema({
    studentNumber: String,
    familyName: String,
    givenName: String,
    middleName: String,
    sex: String,
    civilStatus: String,
    religion: String,
    nationality: String,
    age: Number,
    dateofbirth: String,
    placeofbirth: String,
    cityaddress: String,
    cityaddressTelNumber: String,
    provincialaddress: String,
    provincialaddressTelNumber: String,
    fathername: String,
    mothername: String,
    fatheroccupation: String,
    motheroccupation: String,
    emergencynumber: String,
    student: Boolean,
    parent: Boolean,
    sponsor: Boolean,
    sponsorname: String,
    elemcompleted: String,
    elemcompletedyear: String,
    juniorhighcompleted: String,
    juniorhighcompletedyear: String,
    seniorhighcompleted: String,
    seniorhighcompletedyear: String,
    schoollastattended: String,
    schoollastattendedyear: String,
    honorabledismissal: Boolean,
    tor: Boolean,
    formDocument: Boolean,
    birthCertificate: Boolean,


});

const studentData = mongoose.model('studentData', studentSchema);

export default studentData;