import mongoose from "mongoose";

const curriculumSchema = mongoose.Schema({
    Code: {
        type: String
    },
    Description: {
        type: String
    },
    Lec: {
        type: Number
    },
    Lab: {
        type: Number
    },
    Units: {
        type: Number
    },
    PreRequisite: {
        type: String
    },
    Course: {
        type: String,
    },
    YearLevel: {
        type: String,
    },
    Semester: {
        type: String,
    }
});

const curriculum = mongoose.model('curriculum', curriculumSchema);

export default curriculum;