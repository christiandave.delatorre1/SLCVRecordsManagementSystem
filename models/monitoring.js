import mongoose from 'mongoose';

const monitoringSchema = mongoose.Schema({
    familyName: String,
    givenName: String,
    middleName: String,
    email: String,
    dateRequest: String,
    documentRequest: String,
    dateReceived: String,
    status: String,

})

const monitoring = mongoose.model('monitoring', monitoringSchema);

export default monitoring;

