import mongoose from "mongoose";

const gradeSchema = mongoose.Schema({
    Name: {
        type: String
    },
    Subject: {
        type: String
    },
    SubjectDescription: {
        type: String
    },
    FinalGrades: {
        type: Number
    },
    Units: {
        type: Number
    },
    Remarks: {
        type: String
    },
    Instructors: {
        type: String
    },
    SchoolYear: {
        type: String
    },
    Course: {
        type: String
    },
    YearLevel: {
        type: String
    },
    Semester: { 
        type: String
    }
});

const grades = mongoose.model('grades', gradeSchema);

export default grades;