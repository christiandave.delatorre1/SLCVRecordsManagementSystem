import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import cors from "cors";
import studentRoutes from "./routes/students.js";
// import multer from 'multer';
// import path from 'path';
import uploadRoutes from "./routes/curriculum.js";
import gradeRoutes from "./routes/grades.js";
import monitoringRoutes from "./routes/monitoring.js";
import emailRoutes from "./routes/email.js";
import userRoutes from './routes/user.js';
import { spawn } from "child_process";
import cron from "node-cron";
import * as dotenv from 'dotenv';
dotenv.config();

const app = express();

app.use(cors());
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
// app.use(express.static(path.resolve(__dirname, 'public')));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
// multer.diskStorage({
//     destination: (req, file, cb) => {
//         cb(null,'./public/csv');
//     },
//     filename: (req, file, cb) => {
//         cb(null, file.originalname)
//     }
// }

app.use("/students", studentRoutes);
app.use("/curriculum", uploadRoutes);
app.use("/grades", gradeRoutes);
app.use("/monitoring", monitoringRoutes);
app.use("/send-email", emailRoutes);
app.use("/user", userRoutes);
// Define constants
const DB_NAME = "test";
const ARCHIVE_PATH = `./${DB_NAME}.gzip`;

cron.schedule("0 0 * * *", () => backupMongoDB());

function backupMongoDB() {
  const child = spawn("mongodump", [
    `--uri=${process.env.CONNECTION_URL}`,
    `--db=${DB_NAME}`,
    `--archive=${ARCHIVE_PATH}`,
    "--gzip",
  ]);

  child.stdout.on("data", (data) => {
    console.log("stdout:\n", data);
  });
  child.stderr.on("data", (data) => {
    console.log("stderr:\n", Buffer.from(data).toString());
  });
  child.on("error", (error) => {
    console.log("error:\n", error);
  });
  child.on("exit", (code, signal) => {
    if (code) console.log("Process exit with code:", code);
    else if (signal) console.log("Process killed with signal:", signal);
    else console.log("Backup is successful ✅");
  });
}

const PORT = process.env.PORT || 9999;

await mongoose
  .connect(process.env.CONNECTION_URL)
  .then(() =>
    app.listen(PORT, () => console.log(`Server listening to port ${PORT}`))
  )
  .catch((error) => console.log(`Failed to connect ${error}`));
